//
//  SecondViewController.swift
//  Testing_App
//
//  Created by SW Dev RGTC 2 on 11/7/21.
//

import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet var textFieldValue: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let localStorage = MyLocalStorage()
        
        textFieldValue.text = localStorage.getData(dataKey: "test-key")
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
