//
//  ViewController.swift
//  Testing_App
//
//  Created by SW Dev RGTC 2 on 11/7/21.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func tapNextButton() {
        if let textFieldValue = textField.text {
            if (textFieldValue != "") {
                let localStorage = MyLocalStorage()
                localStorage.saveData(dataValue: textFieldValue, dataKey:"test-key")
                guard let vc = storyboard?.instantiateViewController(identifier: "Second") as? SecondViewController else
                {
                    print("Fail to Navigate")
                    return
                }
                present(vc,animated: true)
            }
            
        }
    }
}

