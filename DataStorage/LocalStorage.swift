//
//  LocalStorage.swift
//  Testing_App
//
//  Created by SW Dev RGTC 2 on 11/7/21.
//

import Foundation
class MyLocalStorage {
    
    func saveData(dataValue: String,dataKey: String) {
        UserDefaults.standard.set(dataValue, forKey: dataKey)
    }
    
    func getData(dataKey: String) -> String {
        
        if let string = UserDefaults.standard.string(forKey: dataKey) {
            return string
        }
        return ""
    }
}
